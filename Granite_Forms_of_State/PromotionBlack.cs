﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Granite_Forms_of_State
{
    public partial class PromotionBlack : Form
    {

        public Form1 form;
        public PromotionBlack(Form1 fm)
        {
            form = fm;
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form1 frm1 = this.Owner as Form1;
            frm1.message = "B";
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form1 frm1 = this.Owner as Form1;
            frm1.message = "N";
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form1 frm1 = this.Owner as Form1;
            frm1.message = "Q";
            this.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form1 frm1 = this.Owner as Form1;
            frm1.message = "R";
            this.Close();
        }


        private void PromotionBlack_Load(object sender, EventArgs e)
        {

        }
    }
}
