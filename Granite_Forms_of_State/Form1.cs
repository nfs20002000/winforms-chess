﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.IO;


namespace Granite_Forms_of_State
{
    public partial class Form1 : Form
    {

        public class Square : Button
        {
            public Piece piece;
            public int hor, ver;
            //public int color;
            public int EP;
            public Square(int col, int i, int j, int ep = 0)
            {
                hor = i;
                ver = j;
                EP = ep;
                if (col == 0)
                    this.BackColor = Color.White;
                else this.BackColor = Color.Gray;
            }
        };

        public string message;

        public string fen = "";

         public Square[,] board = new Square[8, 8];

        static public Square[,] pseudoBoard = new Square[8, 8];

        private string selectedFolder = "";

        public Image chessSprites;

        static public int currPlayer;

        public Square prevButton;

        public abstract class Piece
        {
            public int color;
            public Bitmap picture;
            public bool WasMove = false;
            abstract public bool IsMoving(Square start, Square finish, Square[,] board);
        };

        public class Rook : Piece
        {
            public Rook(int a) : base()
            {
                color = a;
                switch (a)
                {
                    case 0: picture = new Bitmap(Properties.Resources.blackRook); break;
                    case 1: picture = new Bitmap(Properties.Resources.whiteRook); break;
                }
            }
            public override bool IsMoving(Square start, Square finish, Square[,] board)
            {
                if (Horizontal(start, finish, board)
                    || Vertical(start, finish, board))
                    if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                        return true;
                return false;
            }


        };

        public class Knight : Piece
        {

            public Knight(int a) : base()
            {
                color = a;
                switch (a)
                {
                    case 0: picture = new Bitmap(Properties.Resources.blackKnight); break;
                    case 1: picture = new Bitmap(Properties.Resources.whiteKnight); break;
                }
            }
            public override bool IsMoving(Square start, Square finish, Square[,] board)
            {
                if ((start.hor == finish.hor + 1) && (start.ver == finish.ver + 2)
                    || (start.hor == finish.hor + 2) && (start.ver == finish.ver + 1)
                    || (start.hor == finish.hor - 1) && (start.ver == finish.ver + 2)
                    || (start.hor == finish.hor - 2) && (start.ver == finish.ver + 1)
                    || (start.hor == finish.hor + 1) && (start.ver == finish.ver - 2)
                    || (start.hor == finish.hor + 2) && (start.ver == finish.ver - 1)
                    || (start.hor == finish.hor - 2) && (start.ver == finish.ver - 1)
                    || (start.hor == finish.hor - 1) && (start.ver == finish.ver - 2))
                {
                    if (finish.piece == null)
                        if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                            return true;
                    if ((finish.piece != null) && finish.piece.color != start.piece.color)
                        if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                            return true;
                        else return false;
                }
                return false;
            }
        };

        public class Bishop : Piece
        {

            public Bishop(int a) : base()
            {
                color = a;
                switch (a)
                {
                    case 0: picture = new Bitmap(Properties.Resources.blackBishop); break;
                    case 1: picture = new Bitmap(Properties.Resources.whiteBishop); break;
                }
            }
            public override bool IsMoving(Square start, Square finish, Square[,] board)
            {
                if (Diagonal(start, finish, board))
                    if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                        return true;
                return false;
            }
        };

        public class Queen : Piece
        {
            public Queen(int a) : base()
            {
                color = a;
                switch (a)
                {
                    case 0: picture = new Bitmap(Properties.Resources.blackQueen); break;
                    case 1: picture = new Bitmap(Properties.Resources.whiteQueen); break;
                }
            }
            public override bool IsMoving(Square start, Square finish, Square[,] board)
            {
                if (Diagonal(start, finish, board) || Horizontal(start, finish, board) || Vertical(start, finish, board))
                    if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                        return true;
                return false;
            }
        };

        public class King : Piece
        {
            public King(int a) : base()
            {
                color = a;
                switch (a)
                {
                    case 0: picture = new Bitmap(Properties.Resources.blackKing); break;
                    case 1: picture = new Bitmap(Properties.Resources.whiteKing); break;
                }
            }
            public override bool IsMoving(Square start, Square finish, Square[,] board)
            {
                if ((Math.Abs(finish.ver - start.ver) <= 1)
                    && (Math.Abs(finish.hor - start.hor) <= 1)
                    && (start.ver != finish.ver || start.hor != finish.hor))
                {
                    if (board[finish.hor, finish.ver].piece == null)
                        if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                            return true;
                    if
                      (board[finish.hor, finish.ver].piece != null && board[finish.hor, finish.ver].piece.color != board[start.hor, start.ver].piece.color)
                        if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                            return true;
                        else return false;
                }
                else if (Check(FindKing(board), board, currPlayer) != false)
                {
                    //if (Check(board[7, 5], board, currPlayer) != false || Check(board[7, 6], board, currPlayer) != false)
                        if (board[start.hor, start.ver].piece.color == 1)
                        if (board[start.hor, start.ver].piece.WasMove == false && start.hor == 7 && start.ver == 4 && board[7, 7].piece != null && board[7, 7].piece.WasMove == false && board[7, 7].piece is Rook)
                            if (board[7, 5].piece == null && board[7, 6].piece == null)
                                if (board[finish.hor, finish.ver] == board[7, 6])
                                {
                                                if (Check(board[7, 5], board, currPlayer) == false || Check(board[7, 6], board, currPlayer) == false)
                                                    return false;
                                    return true;
                                }
                    if (board[start.hor, start.ver].piece.color == 1)
                        if (board[start.hor, start.ver].piece.WasMove == false && start.hor == 7 && start.ver == 4 && board[7, 0].piece != null && board[7, 0].piece.WasMove == false && board[7, 0].piece is Rook)
                            if (board[7, 1].piece == null && board[7, 2].piece == null && board[7, 3].piece == null)
                                if (board[finish.hor, finish.ver] == board[7, 2])
                                {
                                    if (Check(board[7, 3], board, currPlayer) == false || Check(board[7, 2], board, currPlayer) == false)
                                        return false;
                                    return true;
                                }
                                   
                                
                    if (board[start.hor, start.ver].piece.color == 0)
                        if (board[start.hor, start.ver].piece.WasMove == false && start.hor == 0 && start.ver == 4 && board[0, 0].piece != null && board[0, 0].piece.WasMove == false && board[0, 0].piece is Rook)
                            if (board[0, 1].piece == null && board[0, 2].piece == null && board[0, 3].piece == null)
                                if (board[finish.hor, finish.ver] == board[0, 2])
                                {
                                    if (Check(board[0, 3], board, currPlayer) == false || Check(board[0, 2], board, currPlayer) == false)
                                        return false;
                                            return true;
                                }             
                    if (board[start.hor, start.ver].piece.color == 0)
                        if (board[start.hor, start.ver].piece.WasMove == false && start.hor == 0 && start.ver == 4 && board[0, 7].piece != null && board[0, 7].piece.WasMove == false && board[0, 7].piece is Rook)
                            if (board[0, 5].piece == null && board[0, 6].piece == null)
                                if (board[finish.hor, finish.ver] == board[0, 6])
                                {
                                    if (Check(board[0, 5], board, currPlayer) == false || Check(board[0, 6], board, currPlayer) == false)
                                        return false;
                                            return true;
                                }
                }
                else return false;
                return false;
            }
        };

        public class Pawn : Piece
        {
            public Pawn(int a) : base()
            {
                color = a;
                switch (a)
                {
                    case 0: picture = new Bitmap(Properties.Resources.blackPawn); break;
                    case 1: picture = new Bitmap(Properties.Resources.whitePawn); break;
                }
            }
            public override bool IsMoving(Square start, Square finish, Square[,] board)
            {
                if (finish.hor != start.hor || finish.ver != start.ver)
                {
                    if (board[start.hor, start.ver].piece.color == 1)
                    {
                        if ((start.hor == 6)
                            && (start.ver == finish.ver)
                            && (start.hor - finish.hor == 2)
                            && board[finish.hor, finish.ver].piece == null
                            && board[finish.hor + 1, finish.ver].piece == null)
                            if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                                return true;
                        if (((start.ver == finish.ver)
                               && (start.hor - finish.hor == 1)
                               && board[finish.hor, finish.ver].piece == null)
                               ||
                               ((Math.Abs(start.ver - finish.ver) == 1)
                               && (start.hor - finish.hor == 1)
                               && board[finish.hor, finish.ver].piece != null
                               && board[finish.hor, finish.ver].piece.color == 0)
                               ||
                               ((Math.Abs(start.ver - finish.ver) == 1)
                               && (start.hor - finish.hor == 1)
                               && board[finish.hor, finish.ver].EP == 2
                               && board[finish.hor + 1, finish.ver].piece is Pawn && board[finish.hor + 1, finish.ver].piece.color == 0
                               ))
                            if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                                return true;
                        return false;
                    }
                    else if (board[start.hor, start.ver].piece.color == 0)
                    {
                        if ((start.hor == 1)
                            && (start.ver == finish.ver)
                            && (start.hor - finish.hor == -2)
                            && board[finish.hor, finish.ver].piece == null
                            && board[finish.hor - 1, finish.ver].piece == null)
                            if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                                return true;
                        if ((start.ver == finish.ver)
                             && (start.hor - finish.hor == -1)
                             && board[finish.hor, finish.ver].piece == null
                             ||
                             ((Math.Abs(start.ver - finish.ver) == 1)
                             && (start.hor - finish.hor == -1)
                             && board[finish.hor, finish.ver].piece != null
                             && board[finish.hor, finish.ver].piece.color == 1)
                             ||
                             ((Math.Abs(start.ver - finish.ver) == 1)
                             && (start.hor - finish.hor == -1)
                             && board[finish.hor, finish.ver].EP == 2
                             && board[finish.hor - 1, finish.ver].piece is Pawn && board[finish.hor - 1, finish.ver].piece.color == 1
                             ))
                            if (Protection(this, start, finish, board, pseudoBoard, currPlayer) != false)
                                return true;
                        return false;
                    }
                    return false;
                }
                else return false;
            }
        };

        public void Init()
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    if (board[i, j] != null) board[i, j].Dispose();
                    if (pseudoBoard[i, j] != null) pseudoBoard[i, j].Dispose();
                }
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    pseudoBoard[i, j] = new Square((i + j) % 2, i, j);
                    board[i, j] = new Square((i + j) % 2, i, j);
                    board[i, j].Size = new Size(50, 50);
                    board[i, j].Location = new Point(j * 50, i * 50);
                    board[i, j].Click += new EventHandler(OnFigurePress);
                    this.Controls.Add(board[i, j]);
                }
            board[0, 0].piece = new Rook(0);
            board[0, 7].piece = new Rook(0);
            board[7, 0].piece = new Rook(1);
            board[7, 7].piece = new Rook(1);
            board[0, 1].piece = new Knight(0);
            board[0, 6].piece = new Knight(0);
            board[7, 1].piece = new Knight(1);
            board[7, 6].piece = new Knight(1);
            board[0, 2].piece = new Bishop(0);
            board[0, 5].piece = new Bishop(0);
            board[7, 2].piece = new Bishop(1);
            board[7, 5].piece = new Bishop(1);
            board[0, 3].piece = new Queen(0);
            board[7, 3].piece = new Queen(1);
            board[0, 4].piece = new King(0);
            board[7, 4].piece = new King(1);
            for (int i = 0; i <= 7; i++)
                board[1, i].piece = new Pawn(0);
            for (int i = 0; i <= 7; i++)
                board[6, i].piece = new Pawn(1);
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (board[i, j].piece != null)
                        board[i, j].BackgroundImage = board[i, j].piece.picture;
            currPlayer = 1;
            fen = "rnbqkbnrpppppppp--------------------------------PPPPPPPPRNBQKBNR w KQkq -";
        }

        public void OnFigurePress(object sender, EventArgs e)
        {
            Square pressedButton = sender as Square;
            int flag = prevButton != null && prevButton.BackColor == Color.Yellow ? 1 : 0;
            if (flag == 0) // Выбираем фигуру
            {
                if (pressedButton.piece != null && currPlayer == pressedButton.piece.color)
                {
                    pressedButton.BackColor = Color.Yellow;
                    for (int i = 0; i < 8; i++)
                        for (int j = 0; j < 8; j++)
                            if (pressedButton.piece.IsMoving(pressedButton, board[i, j], board))
                                board[i, j].BackColor = Color.Red;
                }
                prevButton = pressedButton;
            }
            if (flag == 1) // Ходим фигурой
            {
                if ((pressedButton.piece == null || pressedButton.piece.color != currPlayer) && pressedButton.BackColor == Color.Red)
                {
                    // Проверка на превращение пешки
                    if (
                        prevButton.piece is Pawn &&
                        (
                        (prevButton.piece.color == 1 && pressedButton.hor == 0)
                        ||
                        (prevButton.piece.color == 0 && pressedButton.hor == 7)
                        )
                        )
                        Promotion(sender, e, prevButton.piece.color);
                    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //Проверка на двойной ход пешкой
                    if (
                        prevButton.piece is Pawn &&
                        Math.Abs(pressedButton.hor - prevButton.hor) == 2
                        )
                    {
                        if (pressedButton.hor - prevButton.hor < 0)
                            board[pressedButton.hor + 1, pressedButton.ver].EP = 1;
                        else board[pressedButton.hor - 1, pressedButton.ver].EP = 1;

                    }
                    // Взятие на проходе
                    if (pressedButton.EP == 2 && prevButton.piece is Pawn)
                    {
                        if (prevButton.piece.color == 1)
                        {
                            board[pressedButton.hor + 1, pressedButton.ver].piece = null;
                            board[pressedButton.hor + 1, pressedButton.ver].BackgroundImage = null;
                        }
                        else
                        {
                            board[pressedButton.hor - 1, pressedButton.ver].piece = null;
                            board[pressedButton.hor - 1, pressedButton.ver].BackgroundImage = null;
                        }
                    }
                    //Рокировка
                    if (prevButton.piece is King && Math.Abs(prevButton.ver - pressedButton.ver) == 2)
                    {
                        if (pressedButton.hor == 7 && pressedButton.ver == 6)
                        {
                            board[7, 5].piece = board[7, 7].piece;
                            board[7, 5].BackgroundImage = board[7, 5].piece.picture;
                            board[7, 7].piece = null;
                            board[7, 7].BackgroundImage = null;
                        }
                        if (pressedButton.hor == 7 && pressedButton.ver == 2)
                        {
                            board[7, 3].piece = board[7, 0].piece;
                            board[7, 3].BackgroundImage = board[7, 3].piece.picture;
                            board[7, 0].piece = null;
                            board[7, 0].BackgroundImage = null;
                        }
                        if (pressedButton.hor == 0 && pressedButton.ver == 2)
                        {
                            board[0, 3].piece = board[0, 0].piece;
                            board[0, 3].BackgroundImage = board[0, 3].piece.picture;
                            board[0, 0].piece = null;
                            board[0, 0].BackgroundImage = null;
                        }
                        if (pressedButton.hor == 0 && pressedButton.ver == 6)
                        {
                            board[0, 5].piece = board[0, 7].piece;
                            board[0, 5].BackgroundImage = board[0, 5].piece.picture;
                            board[0, 7].piece = null;
                            board[0, 7].BackgroundImage = null;
                        }
                    }
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    pressedButton.piece = prevButton.piece;
                    pressedButton.piece.WasMove = true;
                    pressedButton.BackgroundImage = pressedButton.piece.picture;
                    prevButton.piece = null;
                    prevButton.BackgroundImage = null;
                    prevButton = pressedButton;
                   
                    SwitchPlayer();
                    CloseSteps();
                    ChangeFen(board, ref fen);
                    if (Stalemate(board, currPlayer) == true)
                    {
                        MessageBox.Show("Пат! Ничья.");
                        Init();
                    }
                    if (Checkmate(board, currPlayer) == true)
                    {
                        if (currPlayer == 0)
                            MessageBox.Show("Мат, белые победили! Не сочтите за расизм...");
                        else
                            MessageBox.Show("Мат, чёрные победили!");
                        Init();
                    }
                    if (Check(FindKing(board), board, currPlayer) == false)
                    {
                        FindKing(board).BackColor = Color.Green;
                        MessageBox.Show("Шах!");
                    }
                    for (int i = 2; i < 6; i++)
                        for (int j = 0; j < 8; j++)
                            switch (board[i, j].EP)
                            {
                                case 0: continue; /*break;*/
                                case 1: board[i, j].EP += 1; break;
                                case 2: board[i, j].EP = 0; break;
                            }
                }
                else
                    CloseSteps();
            }
        }

        public void CloseSteps()
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    if (board[i, j].BackColor == Color.Green && board[i, j].piece.color == currPlayer)
                        continue;
                    if ((i + j) % 2 == 0)
                        board[i, j].BackColor = Color.White;
                    else
                        board[i, j].BackColor = Color.Gray;
                }
        }

        public void SwitchPlayer()
        {
            if (currPlayer == 1)
                currPlayer = 0;
            else currPlayer = 1;
        }

        static public bool Horizontal(Square start, Square finish, Square[,] board)
        {
            if (start.hor == finish.hor
                && start.ver != finish.ver)
            {
                for (int k = 1; k <= Math.Abs(finish.ver - start.ver); k++)
                {
                    int verb = finish.ver < start.ver ? start.ver - k : start.ver + k;
                    if (board[start.hor, verb].piece == null) continue;
                    else
                        if ((board[finish.hor, verb].piece.color != board[start.hor, start.ver].piece.color)
                        && (verb == finish.ver))
                        return true;
                    else return false;
                }
                return true;
            }
            else return false;
        }

        static public bool Vertical(Square start, Square finish, Square[,] board)
        {
            if (start.ver == finish.ver
                && start.hor != finish.hor)
            {
                for (int k = 1; k <= Math.Abs(finish.hor - start.hor); k++)
                {
                    int horb = finish.hor < start.hor ? start.hor - k : start.hor + k;
                    if (board[horb, start.ver].piece == null) continue;
                    else
                        if ((board[horb, finish.ver].piece.color != board[start.hor, start.ver].piece.color)
                        && (horb == finish.hor))
                        return true;
                    else return false;
                }
                return true;
            }
            else return false;
        }

        static public bool Diagonal(Square start, Square finish, Square[,] board)
        {
            if (Math.Abs(start.ver - finish.ver) == Math.Abs(start.hor - finish.hor)
                && (start.hor != finish.hor
                && start.ver != finish.ver))
            {
                int horb, verb;
                for (int k = 1; k <= Math.Abs(finish.ver - start.ver); k++)
                {
                    verb = finish.ver < start.ver ? start.ver - k : start.ver + k;
                    horb = finish.hor < start.hor ? start.hor - k : start.hor + k;
                    if (board[horb, verb].piece == null) continue;
                    else
                    if ((board[horb, verb].piece.color != board[start.hor, start.ver].piece.color)
                        && (horb == finish.hor))
                        return true;
                    else return false;
                }
                return true;
            }
            else return false;
        }

        public void Promotion(object sender, EventArgs e, int color)
        {
            if (color == 1)
            {
                PromotionWhite f = new PromotionWhite(this);
                f.Owner = this;
                f.ShowDialog();
                switch (message)
                {
                    case "B": prevButton.piece = new Bishop(1); break;
                    case "N": prevButton.piece = new Knight(1); break;
                    case "Q": prevButton.piece = new Queen(1); break;
                    case "R": prevButton.piece = new Rook(1); break;
                }
            }
            else
            {
                PromotionBlack f = new PromotionBlack(this);
                f.Owner = this;
                f.ShowDialog();
                switch (message)
                {
                    case "B": prevButton.piece = new Bishop(0); break;
                    case "N": prevButton.piece = new Knight(0); break;
                    case "Q": prevButton.piece = new Queen(0); break;
                    case "R": prevButton.piece = new Rook(0); break;
                }
            }
        }

        static public Square FindKing(Square[,] board)
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (board[i, j].piece is King && board[i, j].piece.color == currPlayer)
                        return board[i, j];
            return board[0, 0];
        }

        static public bool Check(Square goal, Square[,] board, int currPlayer)
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    if (ProtectFromEnemyKing(goal, board, currPlayer) != true) return false;
                    if (PawnsUp(goal, board, currPlayer) != true) return false;
                    if ((board[i, j].piece is King || board[i, j].piece is Pawn) && board[i, j].piece.color != currPlayer)
                        continue;
                   
                    if (board[i, j].piece != null && board[i, j].piece.color != currPlayer)
                    {
                        if (board[i, j].piece.IsMoving(board[i, j], goal, board) == true)
                            return false;
                    }
                    
                }
            return true;
        }

        static public bool ProtectFromEnemyKing(Square goal, Square[,] board, int currPlayer)
        {

            if (goal.ver + 1 <= 7) if (board[goal.hor, goal.ver + 1].piece is King) if (board[goal.hor, goal.ver + 1].piece.color != currPlayer) return false;

            if (goal.ver - 1 >= 0) if (board[goal.hor, goal.ver - 1].piece is King) if (board[goal.hor, goal.ver - 1].piece.color != currPlayer) return false;

            if (goal.hor + 1 <= 7) if (board[goal.hor + 1, goal.ver].piece is King) if (board[goal.hor + 1, goal.ver].piece.color != currPlayer) return false;

            if (goal.hor - 1 >= 0) if (board[goal.hor - 1, goal.ver].piece is King) if (board[goal.hor - 1, goal.ver].piece.color != currPlayer) return false;

            if (goal.hor + 1 <= 7 && goal.ver + 1 <= 7) if (board[goal.hor + 1, goal.ver + 1].piece is King) if (board[goal.hor + 1, goal.ver + 1].piece.color != currPlayer) return false;

            if (goal.hor - 1 >= 0 && goal.ver + 1 <= 7) if (board[goal.hor - 1, goal.ver + 1].piece is King) if (board[goal.hor - 1, goal.ver + 1].piece.color != currPlayer) return false;

            if (goal.hor + 1 <= 7 && goal.ver - 1 >= 0) if (board[goal.hor + 1, goal.ver - 1].piece is King) if (board[goal.hor + 1, goal.ver - 1].piece.color != currPlayer) return false;

            if (goal.hor - 1 >= 0 && goal.ver - 1 >= 0) if (board[goal.hor - 1, goal.ver - 1].piece is King) if (board[goal.hor - 1, goal.ver - 1].piece.color != currPlayer) return false;


            return true;
        }

        static public bool PawnsUp(Square goal, Square[,] board, int currPlayer)
        {
            if (currPlayer == 1)
            {
                if (goal.hor - 1 >= 0 && goal.ver + 1 <= 7) if (board[goal.hor - 1, goal.ver + 1].piece is Pawn) if (board[goal.hor - 1, goal.ver + 1].piece.color != currPlayer) return false;
                if (goal.hor - 1 >= 0 && goal.ver - 1 >= 0) if (board[goal.hor - 1, goal.ver - 1].piece is Pawn) if (board[goal.hor - 1, goal.ver - 1].piece.color != currPlayer) return false;
            }
            else
            {
                if (goal.hor + 1 <= 7 && goal.ver + 1 <= 7) if (board[goal.hor + 1, goal.ver + 1].piece is Pawn) if (board[goal.hor + 1, goal.ver + 1].piece.color != currPlayer) return false;
                if (goal.hor + 1 <= 7 && goal.ver - 1 >= 0) if (board[goal.hor + 1, goal.ver - 1].piece is Pawn) if (board[goal.hor + 1, goal.ver - 1].piece.color != currPlayer) return false;
            }
            return true;
        }

        static public bool Protection(Piece piece, Square start, Square goal, Square[,] board, Square[,] pseudoBoard, int currPlayer)
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    pseudoBoard[i, j].piece = null;
                    pseudoBoard[i, j].EP = 0;
                }
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (board[i, j].piece != null) pseudoBoard[i, j].piece = board[i, j].piece;
            pseudoBoard[goal.hor, goal.ver].piece = pseudoBoard[start.hor, start.ver].piece;
            pseudoBoard[start.hor, start.ver].piece = null;
            if (Check(FindKing(pseudoBoard), pseudoBoard, currPlayer) == false)
                return false;
            else return true;
        }

        static public bool Checkmate(Square[,] board, int currPlayer)
        {
            if (Check(FindKing(board), board, currPlayer) == true)
                return false;
            else
            {
                for (int i = 0; i < 8; i++)
                    for (int j = 0; j < 8; j++)
                    
                        if (board[i, j].piece != null && board[i, j].piece.color == currPlayer)
                            for (int m = 0; m < 8; m++)
                                for (int n = 0; n < 8; n++)
                                    if (board[i, j].piece.IsMoving(board[i, j], board[m, n], board))
                                        return false;
                    
            }
            return true;
        }

        static public bool Stalemate(Square[,] board, int currPlayer)
        {
            if (Check(FindKing(board), board, currPlayer) == false)
                return false;
            else
            {
                for (int i = 0; i < 8; i++)
                    for (int j = 0; j < 8; j++)

                        if (board[i, j].piece != null && board[i, j].piece.color == currPlayer)
                            for (int m = 0; m < 8; m++)
                                for (int n = 0; n < 8; n++)
                                    if (board[i, j].piece.IsMoving(board[i, j], board[m, n], board))
                                        return false;

            }
            return true;
        }

        static public void ChangeFen(Square[,] board, ref string fen)
        {
            fen = "";
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if (board[i, j].piece == null)
                    fen += "-"; 
                    if (board[i, j].piece is Pawn)
                        if (board[i, j].piece.color == 1)
                        fen += "P";
                        else fen += "p";
                    if (board[i, j].piece is Knight)
                        if (board[i, j].piece.color == 1)
                        fen += "N"; 
                        else  fen += "n"; 
                    if (board[i, j].piece is Bishop)
                        if (board[i, j].piece.color == 1)
                         fen += "B"; 
                        else fen += "b"; 
                    if (board[i, j].piece is Rook)
                        if (board[i, j].piece.color == 1)
                         fen += "R"; 
                        else  fen += "r"; 
                    if (board[i, j].piece is Queen)
                        if (board[i, j].piece.color == 1)
                         fen += "Q"; 
                        else  fen += "q"; 
                    if (board[i, j].piece is King)
                        if (board[i, j].piece.color == 1)
                         fen += "K";
                        else  fen += "k"; 
                }
            }
            if (currPlayer == 1)
                fen += " w ";
            else fen += " b ";
            if (board[7, 4].piece is King && board[7, 4].piece.color == 1 && board[7, 4].piece.WasMove == false)
            {
                if (board[7, 7].piece is Rook && board[7, 7].piece.color == 1 && board[7, 7].piece.WasMove == false)
                    fen += "K";
                else fen += '-';
                if (board[7, 0].piece is Rook && board[7, 0].piece.color == 1 && board[7, 0].piece.WasMove == false)
                    fen += "Q";
                else fen += '-';
            }
            if (board[0, 4].piece is King && board[0, 4].piece.color == 0 && board[0, 4].piece.WasMove == false)
            {
                if (board[0, 7].piece is Rook && board[0, 7].piece.color == 0 && board[0, 7].piece.WasMove == false)
                    fen += "k";
                else fen += '-';
                if (board[0, 0].piece is Rook && board[0, 0].piece.color == 0 && board[0, 0].piece.WasMove == false)
                    fen += "q ";
                else fen += "- ";
            }
            bool flag = false;
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    if (board[i, j].EP == 1)
                    {
                        fen += i;
                        fen += j;
                        flag = true;
                    }
                }
            if (flag == false) fen += '-';
        }
        public Form1()
        {
            InitializeComponent();
            Init();
        }
        public void Save(string fileName)
        {
            using (StreamWriter writer = new StreamWriter(fileName))
            {
                writer.WriteLine(fen);
            }
        }
        private void save_Click(object sender, EventArgs e)
        {
            saveFileDialog1.InitialDirectory = selectedFolder;
            saveFileDialog1.Filter = "Позиция (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog1.FileName = "Игра.txt";
            DialogResult result = saveFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Save(saveFileDialog1.FileName);
            }
        }
        private void folder_Click(object sender, EventArgs e)
        {
            folderBrowserDialog1.SelectedPath = selectedFolder;
            DialogResult result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                selectedFolder = folderBrowserDialog1.SelectedPath;
                save.Enabled = true;
                open.Enabled = true;
            }
        }
        private void open_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = selectedFolder;
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                Open(openFileDialog1.FileName);
                Transform_position();
            }
        
        }
        public void Open(string fileName)
        {
            using (StreamReader reader = new StreamReader(fileName))
            {
                fen = reader.ReadLine();
            }
        }
        public void Transform_position()
        {
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    board[i, j].piece = null;
                    board[i, j].BackgroundImage = null;
                }
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                {
                    char piece = fen[i * 8 + j];
                    switch (piece)
                    {
                        case '-': break;
                        case 'R': { board[i, j].piece = new Rook(1); break; }
                        case 'r': { board[i, j].piece = new Rook(0); break; }
                        case 'Q': { board[i, j].piece = new Queen(1); break; }
                        case 'q': { board[i, j].piece = new Queen(0); break; }
                        case 'P': { board[i, j].piece = new Pawn(1); break; }
                        case 'p': { board[i, j].piece = new Pawn(0); break; }
                        case 'K': { board[i, j].piece = new King(1); break; }
                        case 'k': { board[i, j].piece = new King(0); break; }
                        case 'B': { board[i, j].piece = new Bishop(1); break; }
                        case 'b': { board[i, j].piece = new Bishop(0); break; }
                        case 'N': { board[i, j].piece = new Knight(1); break; }
                        case 'n': { board[i, j].piece = new Knight(0); break; }
                    }
                }
            for (int i = 0; i < 8; i++)
                for (int j = 0; j < 8; j++)
                    if (board[i, j].piece != null) board[i, j].BackgroundImage = board[i, j].piece.picture;
            if (fen[65] == 'w') currPlayer = 1; else currPlayer = 0;
            if (board[7, 4].piece is King && board[7, 4].piece.color == 1)
            {
                if (board[7, 7].piece is Rook && board[7, 7].piece.color == 1)
                    if (fen[67] != 'K') board[7, 7].piece.WasMove = true; 
                if (board[7, 0].piece is Rook && board[7, 0].piece.color == 1)
                    if (fen[68] != 'Q') board[7, 0].piece.WasMove = true;
            }
            if (board[0, 4].piece is King && board[0, 4].piece.color == 0)
            {
                if (board[0, 7].piece is Rook && board[0, 7].piece.color == 0)
                    if (fen[69] != 'k') board[0, 7].piece.WasMove = true;
                if (board[0, 0].piece is Rook && board[0, 0].piece.color == 0)
                    if (fen[70] != 'q') board[0, 0].piece.WasMove = true;
            }
            if (fen[72] != '-')
                board[Convert.ToInt32(fen[72]) - 48, Convert.ToInt32(fen[73]) - 48].EP = 2;
        }
        private void new_game_Click(object sender, EventArgs e)
        {
                        Init();
        }
        private void draw_offering_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Принять предложение ничьей?", "Подтвердите действие", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                MessageBox.Show("Ничья!");
                Init();
            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show("Соперник желает продолжить игру");
            }
        }

        private void resignation_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Вы уверены, что хотите сдаться?", "Подтвердите действие", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                if (currPlayer == 1)
                MessageBox.Show("Белые сдались, чёрные победили", "\"Без ошибок не может быть блестящих побед\" - Эм. Ласкер");
                else MessageBox.Show("Чёрные сдались, белые победили", "\"Без ошибок не может быть блестящих побед\" - Эм. Ласкер");
                Init();
            }
            else if (result == DialogResult.No)
            {
                MessageBox.Show("Я буду драться до последнего вздоха!");
            }
        }

        private void about_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Система состоит из одного основного проекта - \"Шахматы\". Основанием для разработки является \"Учебный план по программе бакалавриата. Направление 09.03.04 Программная инженерия. Профиль - разработка программно-информационных систем.\" Copyright © Андрианов Данил ", "О программе");
        }

        private void help_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Игра ведётся по правилам вида спорта \"Шахматы\". Для управления используйте левую кнопку мыши. Для осуществления хода нажмите левой кнопкой мыши сначала на клетку с фигурой, которой вы хотите сходить, затем на клетку, на которую вы хотите эту фигуру переместить (такие клетки будут подсвечены красным). После мата или пата автоматически начинается новая игра." +
                "Вы можете сохранять игру или загружать существующую. Для этого необходимо выбрать папку для загрузки или сохранения, нажав на кнопку \"Выбрать папку\". После выбора папки становятся доступны кнопки \"Загрузить игру\" и \"Сохранить игру\"." +
                "Для сохранения игры нажмите на кнопку \"Сохранить игру\", выберите имя игры (папка уже выбрана) и нажмите ОК. " +
                "Для загрузки существующей игры нажмите на кнопку \"Загрузить игру\", выберите из списка нужную вам и нажмите ОК." +
                "При нажатии на кнопку \"Новая игра\" начнётся новая игра." +
                "Вы можете сдаться или предложить ничью, нажав одноимённые кнопки." +
                "Посмотреть информацию о программе можно нажатием на кнопку \"О программе\". Воюйте только за шахматной доской! Приятной игры!", "1984");

        }
    }
}