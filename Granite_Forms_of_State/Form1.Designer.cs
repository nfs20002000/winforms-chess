﻿namespace Granite_Forms_of_State
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.save = new System.Windows.Forms.Button();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.folder = new System.Windows.Forms.Button();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.open = new System.Windows.Forms.Button();
            this.new_game = new System.Windows.Forms.Button();
            this.draw_offering = new System.Windows.Forms.Button();
            this.resignation = new System.Windows.Forms.Button();
            this.about = new System.Windows.Forms.Button();
            this.help = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // save
            // 
            this.save.Enabled = false;
            this.save.Location = new System.Drawing.Point(547, 32);
            this.save.Name = "save";
            this.save.Size = new System.Drawing.Size(107, 23);
            this.save.TabIndex = 0;
            this.save.Text = "Сохранить игру";
            this.save.UseVisualStyleBackColor = true;
            this.save.Click += new System.EventHandler(this.save_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // folder
            // 
            this.folder.Location = new System.Drawing.Point(547, 3);
            this.folder.Name = "folder";
            this.folder.Size = new System.Drawing.Size(107, 23);
            this.folder.TabIndex = 1;
            this.folder.Text = "Выбрать папку";
            this.folder.UseVisualStyleBackColor = true;
            this.folder.Click += new System.EventHandler(this.folder_Click);
            // 
            // open
            // 
            this.open.Enabled = false;
            this.open.Location = new System.Drawing.Point(547, 61);
            this.open.Name = "open";
            this.open.Size = new System.Drawing.Size(107, 23);
            this.open.TabIndex = 2;
            this.open.Text = "Загрузить игру";
            this.open.UseVisualStyleBackColor = true;
            this.open.Click += new System.EventHandler(this.open_Click);
            // 
            // new_game
            // 
            this.new_game.Location = new System.Drawing.Point(547, 90);
            this.new_game.Name = "new_game";
            this.new_game.Size = new System.Drawing.Size(107, 23);
            this.new_game.TabIndex = 3;
            this.new_game.Text = "Новая игра";
            this.new_game.UseVisualStyleBackColor = true;
            this.new_game.Click += new System.EventHandler(this.new_game_Click);
            // 
            // draw_offering
            // 
            this.draw_offering.Location = new System.Drawing.Point(547, 120);
            this.draw_offering.Name = "draw_offering";
            this.draw_offering.Size = new System.Drawing.Size(107, 36);
            this.draw_offering.TabIndex = 4;
            this.draw_offering.Text = "Предложить ничью";
            this.draw_offering.UseVisualStyleBackColor = true;
            this.draw_offering.Click += new System.EventHandler(this.draw_offering_Click);
            // 
            // resignation
            // 
            this.resignation.Location = new System.Drawing.Point(547, 162);
            this.resignation.Name = "resignation";
            this.resignation.Size = new System.Drawing.Size(107, 23);
            this.resignation.TabIndex = 5;
            this.resignation.Text = "Сдаться";
            this.resignation.UseVisualStyleBackColor = true;
            this.resignation.Click += new System.EventHandler(this.resignation_Click);
            // 
            // about
            // 
            this.about.Location = new System.Drawing.Point(547, 201);
            this.about.Name = "about";
            this.about.Size = new System.Drawing.Size(107, 23);
            this.about.TabIndex = 6;
            this.about.Text = "О программе";
            this.about.UseVisualStyleBackColor = true;
            this.about.Click += new System.EventHandler(this.about_Click);
            // 
            // help
            // 
            this.help.Location = new System.Drawing.Point(547, 241);
            this.help.Name = "help";
            this.help.Size = new System.Drawing.Size(107, 23);
            this.help.TabIndex = 7;
            this.help.Text = "Справка";
            this.help.UseVisualStyleBackColor = true;
            this.help.Click += new System.EventHandler(this.help_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 450);
            this.Controls.Add(this.help);
            this.Controls.Add(this.about);
            this.Controls.Add(this.resignation);
            this.Controls.Add(this.draw_offering);
            this.Controls.Add(this.new_game);
            this.Controls.Add(this.open);
            this.Controls.Add(this.folder);
            this.Controls.Add(this.save);
            this.Name = "Form1";
            this.Text = "Шахматы";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button save;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button folder;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button open;
        private System.Windows.Forms.Button new_game;
        private System.Windows.Forms.Button draw_offering;
        private System.Windows.Forms.Button resignation;
        private System.Windows.Forms.Button about;
        private System.Windows.Forms.Button help;
    }
}

